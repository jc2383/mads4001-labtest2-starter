package com.example.lab2starter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;

// 1. Import the correct Toolbar package
import androidx.appcompat.widget.Toolbar;

public class MovieListActivity extends AppCompatActivity {

    final String TAG = "CINEPLEX";

    // data source
    ArrayList<Movie> movies = new ArrayList<Movie>();

    // Shared Preferences
    SharedPreferences prefs;
    public static final String PREFERENCES_NAME = "CineplexSP";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_list);

        // -------------------------------------
        // Initialize Shared Preferences
        // --------------------------------------

        // @TODO: Write code to initialize shared preferences

        // --------------------------------------
        // @HINT:  Get today's calendar date
        // --------------------------------------
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        int currentMonth = calendar.get(Calendar.MONTH);
        int currentDay = calendar.get(Calendar.DAY_OF_MONTH);

        String date = currentYear + "-" + currentMonth + "-" + currentDay;
        Log.d(TAG, "Today's date is: " + date);

        TextView tvCurrentDate = (TextView) findViewById(R.id.tvCurrentDate);

        tvCurrentDate.setText("Today's date is: " + date);


        // --------------------------------------
        // Make some movies and add them to the movies list
        // - You can modify this code as you see fit
        // --------------------------------------
        Movie m1 = new Movie(2020, 1, 1);
        Movie m2 = new Movie(2020, 2, 8);
        Movie m3 = new Movie(2020, 3, 25);
        Movie m4 = new Movie(2020, 4, 17);

        movies.add(m1);
        movies.add(m2);

        // DEBUG: print out the movies to the terminal
        Log.d(TAG, "The movies in the system are:");
        for (int i = 0; i < movies.size(); i++) {
            Movie m = movies.get(i);
            Log.d(TAG, i + ": " + m.getReleaseDate());
        }
        Log.d(TAG, "-----");


        // --------------------------------------
        // ListView & Adapter Code
        // --------------------------------------

        //@TODO: Code for your listview and adapaters


        // HINT: If you want to segue to another screen inside an onClick, you need code like this:
//        Intent i = new Intent(MovieListActivity.this, MovieDetailActivity.class);
//        startActivity(i);


    }

    public void movieDetailButtonPressed(View view) {
        Intent i = new Intent(MovieListActivity.this, MovieDetailActivity.class);
        startActivity(i);
    }


    // Helper function to get all movie in a specific genre
    public ArrayList<Movie> getMoviesByGenre(String genre) {
        ArrayList<Movie> actionMovies = new ArrayList<Movie>();

        final String SEARCH_GENRE = genre;
        for (int i = 0; i < movies.size(); i++) {
            // @TODO: Write the code to search for action movies and add them to the actionMoviesList
        }

        return actionMovies;
    }



}
