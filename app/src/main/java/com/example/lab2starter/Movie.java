package com.example.lab2starter;

public class Movie {

    // You may modify this class as you see fit.
    private int year;
    private int month;
    private int day;
    private String releaseDate;

    public Movie(int year, int month, int day) {
        this.releaseDate = year + "-" + month + day;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }
}

